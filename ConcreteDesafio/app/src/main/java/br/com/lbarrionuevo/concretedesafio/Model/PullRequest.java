package br.com.lbarrionuevo.concretedesafio.Model;

/**
 * Created by casa on 10/04/2017.
 */

public class PullRequest {
    private String pullRequest;
    private String descPullRequest;
    private String user;
    private String nmUser;
    private String urlImage;
    private String htmlUrl;

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public PullRequest(String pullRequest, String descPullRequest, String user, String nmUser, String urlImage, String htmlUrl) {

        this.pullRequest = pullRequest;
        this.descPullRequest = descPullRequest;
        this.user = user;
        this.nmUser = nmUser;
        this.urlImage = urlImage;
        this.htmlUrl = htmlUrl;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getNmUser() {
        return nmUser;
    }

    public void setNmUser(String nmUser) {
        this.nmUser = nmUser;
    }



    public String getPullRequest() {
        return pullRequest;
    }

    public void setPullRequest(String pullRequest) {
        this.pullRequest = pullRequest;
    }

    public String getDescPullRequest() {
        return descPullRequest;
    }

    public void setDescPullRequest(String descPullRequest) {
        this.descPullRequest = descPullRequest;
    }
}
