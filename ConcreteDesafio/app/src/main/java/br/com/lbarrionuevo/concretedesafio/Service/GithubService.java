package br.com.lbarrionuevo.concretedesafio.Service;

import java.util.ArrayList;
import java.util.Map;

import br.com.lbarrionuevo.concretedesafio.Model.PullRequest;

import br.com.lbarrionuevo.concretedesafio.Model.PullRequestJSON;
import br.com.lbarrionuevo.concretedesafio.Model.RepositoryJSON;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by casa on 05/04/2017.
 */

public interface GithubService {
    @GET("search/repositories")
    Call<RepositoryJSON> listRepos(@QueryMap Map< String, String > params);

    @GET("pulls")
    Call<ArrayList<PullRequestJSON>> listPullRequest();
}
