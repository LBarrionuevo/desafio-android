package br.com.lbarrionuevo.concretedesafio.Model;

import android.graphics.Bitmap;

/**
 * Created by casa on 05/04/2017.
 */

public class Repository {


    String nmRepo;
    String descRepo;
    String user;
    String nmUser;
    String urlImage;
    int stars, forks;

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public Repository(String nmRepo, String descRepo, String user, String nmUser, int stars, int forks, String urlImage) {
        this.nmRepo = nmRepo;
        this.descRepo = descRepo;
        this.user = user;
        this.nmUser = nmUser;
        this.stars = stars;
        this.forks = forks;
        this.urlImage = urlImage;
    }



    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }



    public String getNmRepo() {
        return nmRepo;
    }

    public void setNmRepo(String nmRepo) {
        this.nmRepo = nmRepo;
    }

    public String getDescRepo() {
        return descRepo;
    }

    public void setDescRepo(String descRepo) {
        this.descRepo = descRepo;
    }

    public String getNmUser() {
        return nmUser;
    }

    public void setNmUser(String nmUser) {
        this.nmUser = nmUser;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public int getForks() {
        return forks;
    }

    public void setForks(int forks) {
        this.forks = forks;
    }
}
