package br.com.lbarrionuevo.concretedesafio;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import br.com.lbarrionuevo.concretedesafio.Model.PullRequest;
import br.com.lbarrionuevo.concretedesafio.Model.PullRequestJSON;
import br.com.lbarrionuevo.concretedesafio.Service.GithubService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PullRequestActivity extends AppCompatActivity {

    public String API = "https://api.github.com/repos/";
    RecyclerView rvPull;
    private List<PullRequest> pullList = new ArrayList<>();
    private PullAdapter mAdapter;
    PullRequest pullRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);

        rvPull = (RecyclerView) findViewById(R.id.rv_pull);
        mAdapter = new PullAdapter(pullList, this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        rvPull.setLayoutManager(layoutManager);
        rvPull.setHasFixedSize(true);
        rvPull.setItemAnimator(new DefaultItemAnimator());
        rvPull.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        Intent intent = getIntent();

        Bundle bundle = intent.getExtras();

        API = API +  bundle.get("user").toString().toLowerCase()+ "/" + bundle.get("repo").toString().toLowerCase()+ "/";

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();


        GithubService mainService = retrofit.create(GithubService.class);

        Call<ArrayList<PullRequestJSON>> call = mainService.listPullRequest();

        call.enqueue(new Callback<ArrayList<PullRequestJSON>>() {
            @Override
            public void onResponse(Call<ArrayList<PullRequestJSON>> call, Response<ArrayList<PullRequestJSON>> response) {
                if (!response.isSuccessful()) {
                    Log.i("TAG", "Erro!!!!!!!!!: " + response.code());
                    finish();
                } else {

                    if(String.valueOf(response.body()).equals("[]") ) {

                        AlertDialog.Builder alert = new AlertDialog.Builder(PullRequestActivity.this);
                        alert.setTitle("Ops");
                        alert.setMessage("N�o houve retorno de dados");
                        alert.setNeutralButton("Voltar", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });

                        alert.show();

                    }else{
                    ArrayList<PullRequestJSON> pullReq = response.body();

                    for (PullRequestJSON pr: pullReq) {


                        pullRequest = new PullRequest(pr.getTitle(), pr.getBody(), pr.getUser().getLogin(),
                                pr.getBase().getRepo().getDescription(), pr.getUser().getAvatarUrl(),
                                pr.getHtmlUrl());

                        pullList.add(pullRequest);
                    }

                    rvPull.setAdapter(mAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<PullRequestJSON>> call, Throwable t) {
                Log.e( "Erro:", " " + t.getMessage());
                Toast.makeText(PullRequestActivity.this,"Falha na conex�o", Toast.LENGTH_LONG).show();
                finish();
            }



        });


    }
}
